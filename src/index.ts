class Sorter {

  constructor(public collection: number[]) { }

  sort(): void {
    const { length } = this.collection
    for (let i = Math.floor(length / 2) - 1; i >= 0; i--) {
      this.heapify(this.collection, length, i)
    }

    let j = length - 1
    while (j >= 1) {
      this.swap(this.collection, 0, j)
      this.heapify(this.collection, j, 0)
      j--
    }
  }

  private heapify(collection: number[], length: number, i: number) {
    let largest = i

    let leftLeaf = 2 * i + 1
    let rightLeaf = 2 * i + 2

    // if the left child is larger than the current largest
    if (leftLeaf < length && collection[leftLeaf] > collection[largest]) {
      largest = leftLeaf
    }

    // if the right child is larger than the current largest
    if (rightLeaf < length && collection[rightLeaf] > collection[largest]) {
      largest = rightLeaf
    }
    // If the largest of the two is not the original largest
    if (largest != i) {
      // Swap i and the largest.
      this.swap(collection, i, largest);
      // Heapify the sub-tree. 
      this.heapify(collection, length, largest);
    }
  }

  private swap(arr: number[], a: number, b: number) {
    const tmp = arr[a];
    arr[a] = arr[b];
    arr[b] = tmp;
  }
}


const sorter = new Sorter([10, 3, -5, 0])

sorter.sort()
console.log(sorter.collection)